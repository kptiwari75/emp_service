from message_handler import MessageHandler
from trigger.department_manager_list_handler import get_department_manager_list
from trigger.department_list_handler import get_department_list


dept_list_payload = {
    "trigger": "get_dept_list",
    "project_name": "emp_service"
}

dept_manager_list_payload = {
    "trigger": "get_dept_manager_list",
    "project_name": "emp_service"
}

MessageHandler.handle_message(dept_list_payload)
MessageHandler.handle_message(dept_manager_list_payload)