from producer import RabbitMq


def get_department_manager_list(msg_dict):
    message = dict()
    message["r_key"] = "dept.dept_manager_list"
    message["trigger"] = "dept_manager_list"
    message["project_name"] = "employee_service"

    server = RabbitMq(host="localhost", queue="dept", exchange="test", exchange_type="topic",
                      #routing_key=message["r_key"],
                      routing_key="dept.{}".format(message["trigger"]),
                      durable=True)
    server.publish(payload=message)