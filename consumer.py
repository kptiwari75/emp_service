from message_handler import MessageHandler

try:
    import pika
    import json
    import ast
    from employee.emp import empDB
except Exception as e:
    print("Some modules are missing")


class RabbitMq(object):
    def __init__(self, host="localhost", queue="krishna", exchange="", exchange_type="", routing_key="", durable=True,
                 auto_ack=True):
        self.host = host
        self.queue = queue
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.routing_key = routing_key
        self.durable = durable
        self.auto_ack = auto_ack
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(host=host)
                                                   )
        self._channel = self._connection.channel()

        self._channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type, durable=self.durable)
        self.result = self._channel.queue_declare(queue=self.queue)
        self.queue_name = self.result.method.queue
        self._channel.queue_bind(exchange=self.exchange, queue=self.queue_name, routing_key=self.routing_key)

    def callback(ch, method, properties, body):
        data = body
        data = data.decode("utf-8")
        data = ast.literal_eval(data)

        for each in empDB:
            if "dept_list" not in each and "dept_list" in data:
                each["dept_list"] = data["dept_list"]
            if "dept_manager_list" not in each and "dept_manager_list" in data:
                each["dept_manager_list"] = data["dept_manager_list"]
        print(empDB)
        MessageHandler.handle_message(data)

    def consume(self):
        print(' [*] Waiting for logs. To exit press CTRL+C')
        self._channel.basic_consume(
            queue=self.queue_name, on_message_callback=RabbitMq.callback, auto_ack=self.auto_ack)
        self._channel.start_consuming()


if __name__ == "__main__":
    try:
        server = RabbitMq(host="localhost", queue="emp", exchange="test", exchange_type="topic", routing_key="emp.*",
                          durable=True, auto_ack=True)
        server.consume()
    except Exception as e:
        print(e)
