from trigger.department_manager_list_handler import get_department_manager_list
from trigger.department_list_handler import get_department_list


class MessageHandler:
    @staticmethod
    def handle_message(message_json):
        MessageHandler.get_trigger_handler(message_json)

    @staticmethod
    def get_trigger_handler(msg_dict):
        print(msg_dict)
        trigger = msg_dict.get('trigger',None)
        try:
            if trigger == "get_dept_list":
                get_department_list(msg_dict)

            if trigger == "get_dept_manager_list":
                get_department_manager_list(msg_dict)

        except Exception as e:
            print("Invalid usage error. Check your request." + str(e))
