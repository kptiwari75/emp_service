try:
    import pika
except Exception as e:
    print("Some modules are missing")


class RabbitMq(object):
    def __init__(self, host="localhost", queue="krishna", exchange="", exchange_type="", routing_key="", durable=True):
        self.host = host
        self.queue = queue
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.routing_key = routing_key
        self.durable = durable
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(host=host)
                                                   )
        self._channel = self._connection.channel()

        self._channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type, durable=self.durable)
        self._channel.queue_declare(queue=self.queue)

    def publish(self, payload={}):
        self._channel.basic_publish(exchange=self.exchange,
                                    routing_key=self.routing_key,
                                    body=str(payload))
        print("published ...")
        self._connection.close()

